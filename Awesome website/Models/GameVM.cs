﻿using System.Collections.Generic;

namespace Awesome_website.Models
{
    public class GameVM
    {
        /// <summary>
        /// Title of the game
        /// </summary>
        public string Title { get; set; }

        /// <summary>
        /// Html file of the game
        /// </summary>
        public string Html { get; set; }

        /// <summary>
        /// Js files of the game
        /// </summary>
        public List<string> JsFiles { get; set; }

        /// <summary>
        /// cssFiles of the game
        /// </summary>
        public List<string> CssFiles { get; set; }

        /// <summary>
        /// Images from the game
        /// </summary>
        public List<string> ImageFiles { get; set; }
    }
}