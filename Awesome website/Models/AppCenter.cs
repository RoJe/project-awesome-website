﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Microsoft.AspNet.Identity;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using Awesome_website.Helpers.Database;
using Awesome_website.Helpers.Serialize;

namespace Awesome_website.Models
{
    public class AppCenter
    {
        /// <summary>
        /// The amount of of containers thet should be generated
        /// </summary>
        private static int GRID_SIZE;
        /// <summary>
        /// The amount of nodes that should be inside the container.
        /// </summary>
        private static int NODE_COUNT;

        /// <summary>
        /// Class with functionality specificually for user database ralated things
        /// </summary>
        private DatabaseUserHelper user;

        /// <summary>
        /// Class with functionallity specificually for game database rlated things
        /// </summary>
        private DatabaseGameHelper games;

        /// <summary>
        /// Class with functionallity specificually for website database related things.
        /// </summary>
        private DatabaseWebsitheelper websites;

        /// <summary>
        /// Serialize class provided bij visual studio
        /// </summary>
        private SerializeListHelper<AppCenterGrid> serializer;

        public AppCenter()
        {
            //Assign the value for the grid size
            GRID_SIZE = 4;

            // Amount of nodes that should go inside the container
            NODE_COUNT = 6;

            // Initialize the classes for the database thiings.
            this.user = new DatabaseUserHelper(new Entities());
            this.games = new DatabaseGameHelper(new Entities());
            this.websites = new DatabaseWebsitheelper(new Entities());
            this.serializer = new SerializeListHelper<AppCenterGrid>();


            this.checkUserGrid();
        }

        /// <summary>
        /// Get the available games from the database.
        /// </summary>
        /// <returns>List<> With games</returns>
        public List<Game> GetGames()
        {
            return games.getGames();
        }
        /// <summary>
        /// Get the available websites from the database.
        /// </summary>
        /// <returns>List<> with websites</returns>
        public List<AppGridApplication> GetWebsites()
        {
            return websites.getWebsites();
        }
        /// <summary>
        /// If the user selected a node he/she want to populated, we update it in the database 
        /// </summary>
        /// <param name="appId">The id from the app from the database.</param>
        /// <param name="isWebsite">Is it a website or a games? Simple way of doing it but it works</param>
        /// <param name="nodePosition">The ID of the node that has been clicked on relative to the position from the databse.</param>
        public void updateGrid(int appId, bool isWebsite, int nodePosition)
        {
            List<AppCenterGrid> appgrid = this.serializer.deSerializeList(this.user.getUserData().appcenter_grid);
            appgrid[nodePosition].appId = appId;
            appgrid[nodePosition].name = (isWebsite ? "website" : "game");
            this.user.saveUserGrid(this.serializer.serializeList(appgrid));
        }
        /// <summary>
        /// Check if the user has grid saved in the database, If its to big or to small and updated where need be.
        /// </summary>
        private void checkUserGrid()
        {

            if (this.user.getUserData().appcenter_grid == "123" || this.user.getUserData().appcenter_grid == null)
            {
                this.insertEmptyGrid();
            }
            else if (this.serializer.deSerializeList(this.user.getUserData().appcenter_grid).Count() < (GRID_SIZE * NODE_COUNT))
            {
                List<AppCenterGrid> appgrid = this.serializer.deSerializeList(this.user.getUserData().appcenter_grid);
                int curSize = appgrid.Count();
                int toAdd = (GRID_SIZE * NODE_COUNT) - curSize;

                this.expandGrid(appgrid, toAdd);
            }
            else if (this.serializer.deSerializeList(this.user.getUserData().appcenter_grid).Count() > (GRID_SIZE * NODE_COUNT))
            {
                List<AppCenterGrid> appgrid = this.serializer.deSerializeList(this.user.getUserData().appcenter_grid);
                int curSize = appgrid.Count();
                int toRemove = curSize - (GRID_SIZE * NODE_COUNT);

                this.shrinkGrid(appgrid, toRemove);
            }

        }

        /// <summary>
        /// Insert an empty grid in the user table (AspNetUsers)
        /// </summary>
        private void insertEmptyGrid()
        {
            List<AppCenterGrid> dummyGrid = new List<AppCenterGrid>();
            for (int i = 0; i < (GRID_SIZE * NODE_COUNT); i++)
            {
                dummyGrid.Add(new AppCenterGrid { name = "", appId = 1 });
            }

            try
            {
                this.user.saveUserGrid(this.serializer.serializeList(dummyGrid));
            }
            catch (InvalidCastException e)
            {
                throw new Exception(e.ToString());
            }

        }

        /// <summary>
        /// Increase the size of the grid if need be.
        /// </summary>
        /// <param name="appgrid">The current appgrid</param>
        /// <param name="toAdd">How many we need to add</param>
        private void expandGrid(List<AppCenterGrid> appgrid, int toAdd)
        {
            for (int i = 0; i < toAdd; i++)
            {
                appgrid.Add(new AppCenterGrid { name = "", appId = 1 });
            }

            try
            {
                this.user.saveUserGrid(this.serializer.serializeList(appgrid));
            }
            catch (InvalidCastException e)
            {
                throw new Exception(e.ToString());
            }
        }

        /// <summary>
        /// Decrease the size of the app grid if need be.
        /// </summary>
        /// <param name="appgrid">The current appgrid</param>
        /// <param name="toRemove">How many we need to remove</param>
        private void shrinkGrid(List<AppCenterGrid> appgrid, int toRemove)
        {
            int listMax = appgrid.Count() - 1;
            int listMin = (GRID_SIZE * NODE_COUNT) - 1;

            for (int i = listMax; i > listMin; i--)
            {
                appgrid.RemoveAt(i);
            }

            try
            {
                this.user.saveUserGrid(this.serializer.serializeList(appgrid));
            }
            catch (InvalidCastException e)
            {
                throw new Exception(e.ToString());
            }
        }


        /// <summary>
        /// Generate a list of, in this order, each container > child nodes > populated information ( game/website, name, link to etc.. )
        /// </summary>
        /// <returns>Populated list of the above explained information</returns>
        public List<List<AppGridApplication>> getGridLayout()
        {
            List<AppCenterGrid> appgrid = this.serializer.deSerializeList(this.user.getUserData().appcenter_grid);
            List<List<AppGridApplication>> newAppGrid = new List<List<AppGridApplication>>();

            for (int i = 0; i < GRID_SIZE; i++)
            {
                List<AppGridApplication> dummyGrid = new List<AppGridApplication>();
                for (int v = 0; v < NODE_COUNT; v++)
                {
                    int id = appgrid[((i * NODE_COUNT) + v)].appId;
                    string name = appgrid[((i * NODE_COUNT) + v)].name;
                    
                    switch (name)
                    {
                        case "":
                            dummyGrid.Add(this.user.database.AppGridApplications.SingleOrDefault(x => x.Id == id));
                            break;
                        case "website":
                            dummyGrid.Add(this.user.database.AppGridApplications.SingleOrDefault(x => x.Id == id));
                            break;
                        case "game":
                            var gameDummy = this.user.database.Games.SingleOrDefault(x => x.Id == id);
                            dummyGrid.Add(new AppGridApplication { name = gameDummy.Title, link = "/Games/" + gameDummy.Slug + "/", Id = gameDummy.Id, icon = "fa fa-home", description = "a video game" });
                            break;
                        default:
                            break;

                    }
                }
                newAppGrid.Add(dummyGrid);
                
            }

            return newAppGrid;
        }
    }
}