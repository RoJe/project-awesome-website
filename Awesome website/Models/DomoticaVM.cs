﻿namespace Awesome_website.Models
{
    public class DomoticaVM
    {
        /// <summary>
        /// Amount of lamps within the application
        /// </summary>
        public int LampCount { get; set; }
        /// <summary>
        /// Amount of shutters within the application
        /// </summary>
        public int ShutterCount { get; set; }

        /// <summary>
        /// Status the lamps
        /// </summary>
        public bool[] LampStatus { get; set; }
        /// <summary>
        /// Status of the windows/shutters
        /// </summary>
        public bool[] WindowStatus { get; set; }

        /// <summary>
        /// The current temperature
        /// </summary>
        public double HeaterTemperature { get; set; }

        /// <summary>
        /// Error ( if any )
        /// </summary>
        public string Error { get; set; }

        public DomoticaVM(int lampCount, int shutterCount)
        {
            LampCount = lampCount;
            ShutterCount = shutterCount;

            LampStatus = new bool[LampCount];
            WindowStatus = new bool[ShutterCount];
        }
    }
}