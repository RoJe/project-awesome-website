﻿using System;
using System.IO;
using System.Net.Sockets;
using Awesome_website.Services;

namespace Awesome_website
{
    /// <summary>
    /// Wrapper class for DaHaus
    /// </summary>
    /// <date>14-11-2017</date>
    public class DaHaus : IDisposable
    {
        /// <summary>
        /// The client that connects to the DaHaus server
        /// </summary>
        private TcpClient _client;

        /// <summary>
        /// Stream for sending data
        /// </summary>
        private StreamWriter _output;

        /// <summary>
        /// Stream for receiving data
        /// </summary>
        private StreamReader _input;

        /// <summary>
        /// Prevent double disposals
        /// </summary>
        private bool _disposed;


        /// <summary>
        /// Connect to the DaHaus server and init input and output
        /// </summary>
        public DaHaus()
        {
            var settingsService = new SettingsService();

            _client = new TcpClient(settingsService.GetSetting("DaHausHost"), Convert.ToInt32(settingsService.GetSetting("DaHausPort"))); 
            _output = new StreamWriter(_client.GetStream());
            _input = new StreamReader(_client.GetStream());

            _disposed = false;
        }

        #region Lamps

        /// <summary>
        /// Set the state of a lamp
        /// </summary>
        /// <param name="id">id of the lamp</param>
        /// <param name="state">On / Off</param>
        /// <returns>Current state of the lamp</returns>
        public bool SetLamp(int id, bool state)
        {
            string command = $"lamp {id} " + ((state) ? "on" : "off");

            // Expected format: Lamp [id] is [on/off]
            string response = SendCommand(command, readOutput: true);

            return GetLampStatus(response);
        }

        /// <summary>
        /// Get the current status of a lamp
        /// </summary>
        /// <param name="id">The id of the lamp</param>
        /// <returns>Current state of the lamp</returns>
        public bool GetLampStatus(int id)
        {
            string command = $"lamp {id}";

            // Expected format: Lamp [id] is [on/off]
            string response = SendCommand(command, readOutput: true);

            return GetLampStatus(response);
        }

        /// <summary>
        /// Get the amount of lamps
        /// </summary>
        ///// <returns>The amount of lamps</returns>
        public int GetLampCount()
        {
            string command = "lamps";

            string response = SendCommand(command, readOutput: true);

            return int.Parse(response);
        }

        /// <summary>
        /// Toggles the state of a lamp
        /// </summary>
        /// <param name="id">The id of the lamp</param>
        /// <returns>Current state of the lamp</returns>
        public bool ToggleLamp(int id)
        {
            return SetLamp(id, !GetLampStatus(id));
        }

        /// <summary>
        /// Converts the response from DaHaus (string) to the status off the lamp (bool)
        /// Expects a string in this format: lamp [id] is [on/off]
        /// </summary>
        /// <param name="response">The response from DaHaus</param>
        /// <returns>Lamp state</returns>
        private bool GetLampStatus(string response)
        {
            return response.ToLower().Contains("on");
        }

        #endregion

        #region Windows

        /// <summary>
        /// Set a window to open / close
        /// </summary>
        /// <param name="id">id of the window</param>
        /// <param name="state">Open / Close</param>
        /// <returns>Current state of the window</returns>
        public bool SetWindow(int id, bool state)
        {
            string command = $"window {id} " + ((state) ? "open" : "close");

            // Expected format: Lamp [id] is [on/off]
            string response = SendCommand(command, readOutput: true);

            return GetWindowStatus(response);
        }

        /// <summary>
        /// Get the current status of a lamp
        /// </summary>
        /// <param name="id">The id of the lamp</param>
        /// <returns>Current state of the lamp</returns>
        public bool GetWindowStatus(int id)
        {
            string command = $"window {id}";

            // Expected format: Lamp [id] is [on/off]
            string response = SendCommand(command, readOutput: true);

            return GetWindowStatus(response);
        }

        /// <summary>
        /// Get the amount of windows
        /// </summary>
        /// <returns>The amount of windows</returns>
        public int GetWindowCount()
        {
            string command = "windows";

            string response = SendCommand(command, readOutput: true);

            return int.Parse(response);
        }

        /// <summary>
        /// Toggles a window
        /// </summary>
        /// <param name="id">The id of the window</param>
        /// <returns>Currernt state of the window</returns>
        public bool ToggleWindow(int id)
        {
            return SetWindow(id, !GetWindowStatus(id));
        }

        /// <summary>
        /// Converts the response from DaHaus (string) to the status off the window (bool)
        /// Expects a string in this format: window [id] is [open/closed]
        /// </summary>
        /// <param name="response">The response from DaHaus</param>
        /// <returns>Window state</returns>
        private bool GetWindowStatus(string response)
        {
            return response.ToLower().Contains("open");
        }

        #endregion

        #region Heater

        public double SetHeater(double temperature)
        {
            string command = $"heater {temperature.ToString().Replace(".", ",")}";

            string response = SendCommand(command, readOutput: true);

            return GetHeaterTemperature(response);
        }

        public double GetHeaterTemperature()
        {
            string command = "heater";

            string response = SendCommand(command, readOutput: true);

            return GetHeaterTemperature(response);
        }

        /// <summary>
        /// Converts the response from DaHaus (string) to the temperature (double)
        /// Expected format: Heater is set to [temp]
        /// </summary>
        /// <param name="response">Response from DaHaus</param>
        /// <returns>Temperature</returns>
        private double GetHeaterTemperature(string response)
        {
            string[] splitted = response.Split(' ');

            return double.Parse(splitted[4]);
        }

        #endregion

        /// <summary>
        /// Close the connection to DaHaus
        /// </summary>
        public void Dispose()
        {
            if (_disposed)
            {
                return;               
            }


            // DaHaus will send a message after the exit command, if we close the connection immediately after sending 'exit' DaHaus will try to write to a socket that has been closed 
            // That's why readOutput has to be true, it will wait for the message that DaHaus returns
            SendCommand("exit", readOutput: true);

            _input.Close();

            _output.Close();

            _client.Close();

            _disposed = true;
        }

        /// <summary>
        /// Sends a command to DaHaus and optionally returns the output
        /// </summary>
        /// <param name="cmd">The command to send</param>
        /// <param name="readOutput">Should we read and return the output?</param>
        /// <returns>The output if readOutput is true, otherwise an empty string</returns>
        private string SendCommand(string cmd, bool readOutput = false)
        {

            // Write the command
            _output.Write(cmd + "\r\n");
            _output.Flush(); // Send to server

            if (readOutput)
            {
                return _input.ReadLine();
            }

            return String.Empty;
        }

    }
}
