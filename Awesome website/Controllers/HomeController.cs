﻿using System.Web.Mvc;
using Awesome_website.Models;

namespace Awesome_website.Controllers
{
    [Authorize]
    public class HomeController : Controller
    {
        /// <summary>
        /// pass the appCenter model to the view so we can use it there.
        /// </summary>
        /// <returns>The view with the appcenter class</returns>
        public ActionResult Index()
        {
            AppCenter appCenter = new AppCenter();

            return View(appCenter);
        }

        /// <summary>
        /// When the user has selected an items to populated the node, update its value inside the database.
        /// </summary>
        /// <param name="appId">The id of the app</param>
        /// <param name="isWebsite">If it's either a game or a website</param>
        /// <param name="nodePosition">The position of the node relative to the database</param>
        /// <returns></returns>
        [HttpPost]
        public JsonResult Add(int appId, bool isWebsite, int nodePosition)
        {
            AppCenter appCenter = new AppCenter();

            appCenter.updateGrid(appId, isWebsite, nodePosition);
            return Json(new { success = true });
        }
    }
}