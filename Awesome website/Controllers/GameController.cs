﻿using System.Linq;
using System.Web.Mvc;
using Awesome_website.Models;
using Awesome_website.Services;

namespace Awesome_website.Controllers
{
    [Authorize]
    public class GameController : Controller
    {
        private readonly GameService _gameService;

        public GameController()
        {
            _gameService = new GameService();
        }

        /// <summary>
        /// show the correct game that the user is requesting
        /// </summary>
        /// <param name="slug">the simple, friendly name within the database</param>
        /// <returns>the game.</returns>
        public ActionResult Index(string slug)
        {
            using (var db = new Entities())
            {
                Game game = db.Games.FirstOrDefault(g => g.Slug.Equals(slug));

                if (game == null)
                {
                    return NotFound();
                }

                GameVM viewModel = _gameService.CollectGameData(game);

                if (viewModel == null)
                {
                    return NotFound();
                }

                return View(viewModel);
            }

        }
        /// <summary>
        /// If the game isn't found return NotFound
        /// </summary>
        /// <returns>View for the found</returns>
        public ActionResult NotFound()
        {
            return View("NotFound");
        }
    }
}