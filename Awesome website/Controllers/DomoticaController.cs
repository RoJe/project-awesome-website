﻿using System;
using System.Net;
using System.Web.Mvc;
using Awesome_website.Services;

namespace Awesome_website.Controllers
{
    [Authorize]
    public class DomoticaController : Controller
    {
        private readonly DomoticaService _domoticaService;

        public DomoticaController()
        {
            _domoticaService = new DomoticaService();            
        }

        // GET: Domotica
        public ActionResult Index()
        {
            return View(_domoticaService.GetVM());
        }

        [HttpPost]
        public JsonResult SetLamp(int id, bool state)
        {
            bool newState = false;

            try
            {
                using (var daHaus = new DaHaus())
                {
                    newState = daHaus.SetLamp(id, state);
                }
            }
            catch (Exception ex)
            {
                Response.StatusCode = (int) HttpStatusCode.InternalServerError;
                return Json(new { error = ex.Message });
            }

            return Json(new
            {
                lampId = id,
                newState = newState
            });
        }

        [HttpPost]
        public JsonResult SetWindow(int id, bool state)
        {
            bool newState = false;

            try
            {
                using (var daHaus = new DaHaus())
                {
                    newState = daHaus.SetWindow(id, state);
                }
            }
            catch (Exception ex)
            {
                Response.StatusCode = (int) HttpStatusCode.InternalServerError;
                return Json(new { error = ex.Message });
            }

            return Json(new
            {
                windowId = id,
                newState = newState
            });
        }

        [HttpPost]
        public JsonResult SetTemperature(double temperature)
        {
            double newTemp;

            try
            {
                using (var daHaus = new DaHaus())
                {
                    newTemp = daHaus.SetHeater(temperature);
                }
            }
            catch (Exception ex)
            {
                Response.StatusCode = (int) HttpStatusCode.InternalServerError;
                return Json(new { error = ex.Message });
            }

            return Json(new
            {
                newTemp = newTemp
            });
        }
    }
}