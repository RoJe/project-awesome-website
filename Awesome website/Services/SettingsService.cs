﻿using System.Configuration;

namespace Awesome_website.Services
{
    public class SettingsService
    {
        /// <summary>
        /// Get a setting from the web.config file
        /// </summary>
        /// <param name="key">Key of the setting</param>
        /// <returns>Value of the setting</returns>
        public string GetSetting(string key)
        {
            return ConfigurationManager.AppSettings[key]; // Get from web.config
        }

    }
}