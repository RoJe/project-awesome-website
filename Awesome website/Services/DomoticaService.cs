﻿
using System.Net.Sockets;
using Awesome_website.Models;

namespace Awesome_website.Services
{
    public class DomoticaService
    {
        private const int LAMP_COUNT = 5;
        private const int WINDOW_COUNT = 2;

        /// <summary>
        /// Connect to DaHaus and collect the current state
        /// </summary>
        /// <returns>Domotica view model</returns>
        public DomoticaVM GetVM()
        {
            DomoticaVM vm = new DomoticaVM(LAMP_COUNT, WINDOW_COUNT);

            try
            {

                using (var daHaus = new DaHaus())
                {
                    for (int lampIndex = 0; lampIndex < LAMP_COUNT; lampIndex++)
                    {
                        vm.LampStatus[lampIndex] = daHaus.GetLampStatus(lampIndex);
                    }

                    for (int shutterIndex = 0; shutterIndex < WINDOW_COUNT; shutterIndex++)
                    {
                        vm.WindowStatus[shutterIndex] = daHaus.GetWindowStatus(shutterIndex);
                    }

                    vm.HeaterTemperature = daHaus.GetHeaterTemperature();
                }
            }
            catch (SocketException ex)
            {
                vm.Error = $"Kon niet verbinden met DaHaus: {ex.Message}";
            }

            return vm;
        }
    }
}