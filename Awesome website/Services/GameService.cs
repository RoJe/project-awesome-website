﻿using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web.Hosting;
using Awesome_website.Models;

namespace Awesome_website.Services
{
    public class GameService
    {

        /// <summary>
        /// 
        /// </summary>
        /// <param name="game"></param>
        /// <returns>The GameVM</returns>
        public GameVM CollectGameData(Game game)
        {
            var vm = new GameVM();

            // Check if game exists
            if (!Directory.Exists($"{HostingEnvironment.ApplicationPhysicalPath}/Games/{game.Slug}"))
            {
                return null;
            }

            vm.Title = game.Title;
            vm.Html = File.ReadAllText($"{HostingEnvironment.ApplicationPhysicalPath}/Games/{game.Slug}/index.html");

            CollectJsFiles(ref vm, ref game);
            CollectCssFiles(ref vm, ref game);
            CollectImageFiles(ref vm, ref game);


            return vm;
        }

        // https://stackoverflow.com/questions/5203945/getting-project-root-path-in-controller-asp-net-mvc
        private void CollectJsFiles(ref GameVM vm, ref Game game)
        {
            string[] jsFiles = Directory.GetFiles($"{HostingEnvironment.ApplicationPhysicalPath}/Games/{game.Slug}", "*.js", SearchOption.AllDirectories);

            vm.JsFiles = jsFiles.ToList();
        }

        private void CollectCssFiles(ref GameVM vm, ref Game game)
        {
            string[] cssFiles = Directory.GetFiles($"{HostingEnvironment.ApplicationPhysicalPath}/Games/{game.Slug}", "*.css", SearchOption.AllDirectories);

            vm.CssFiles = cssFiles.ToList();
        }

        private void CollectImageFiles(ref GameVM vm, ref Game game)
        {
            string[] types = { "jpg", "jpeg", "png", "gif", "tiff", "bmp" };

            vm.ImageFiles = new List<string>();

            foreach (var type in types)
            {
               vm.ImageFiles.AddRange(Directory.GetFiles($"{HostingEnvironment.ApplicationPhysicalPath}/Games/{game.Slug}", $"*.{type}", SearchOption.AllDirectories).ToList());   
            }

        }

    }
}