﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(Awesome_website.Startup))]
namespace Awesome_website
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
