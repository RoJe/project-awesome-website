﻿using System.Web;

namespace Awesome_website.Helpers
{
    public static class PathHelper
    {
        //https://stackoverflow.com/questions/3164/absolute-path-back-to-web-relative-path
        public static string RelativePath(string path, HttpRequest context)
        {
            return path.Replace(context.ServerVariables["APPL_PHYSICAL_PATH"], "").Replace(@"\", "/");
        }
    }
}