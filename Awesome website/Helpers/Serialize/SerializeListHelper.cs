﻿using System.Collections.Generic;
using System.Web.Script.Serialization;

namespace Awesome_website.Helpers.Serialize
{
    /// <summary>
    /// a template class from construction and destruction serialized lists
    /// </summary>
    /// <typeparam name="T">Type of list</typeparam>
    public class SerializeListHelper<T>
    {
        private JavaScriptSerializer serializer;
        public SerializeListHelper()
        {
            this.serializer = new JavaScriptSerializer();
        }

        /// <summary>
        /// Serialize a Data List to a string that is ready for the database.
        /// </summary>
        /// <param name="toSerialize"></param>
        /// <returns></returns>
        public string serializeList(List<T> toSerialize)
        {
            return this.serializer.Serialize(toSerialize);
        }

        /// <summary>
        /// De Serialize a string to a Data List that is ready for use.
        /// </summary>
        /// <param name="serializedString"></param>
        /// <returns></returns>
        public List<T> deSerializeList(string serializedString)
        {
            return this.serializer.Deserialize<List<T>>(serializedString);
        }
    }
}