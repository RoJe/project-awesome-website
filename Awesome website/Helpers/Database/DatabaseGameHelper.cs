﻿using System.Collections.Generic;
using System.Linq;
using Awesome_website.Models;

namespace Awesome_website.Helpers.Database
{
    /// <summary>
    /// A database hlper with functionallity related to games.
    /// </summary>
    public class DatabaseGameHelper : DatabaseHelper
    {
        public DatabaseGameHelper(Entities db) : base(db)
        {
        }

        /// <summary>
        /// Return a list of available games from the database.
        /// </summary>
        /// <returns>List of available games from the database</returns>
        public List<Game> getGames()
        {
            return this.database.Games.ToList();
        }
        
    }
}