﻿using Awesome_website.Models;

namespace Awesome_website.Helpers.Database
{   
    /// <summary>
    /// A helper parnt class with main database functionallity.
    /// </summary>
    public class DatabaseHelper
    {
        public Entities database { get; set; }
        public DatabaseHelper(Entities db)
        {
            this.database = db;
        }
    }
}