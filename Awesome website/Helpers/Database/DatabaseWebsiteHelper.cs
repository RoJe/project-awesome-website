﻿using System.Collections.Generic;
using System.Linq;
using Awesome_website.Models;


namespace Awesome_website.Helpers.Database
{
    /// <summary>
    /// helper class for database related things
    /// </summary>
    public class DatabaseWebsitheelper : DatabaseHelper
    {
        public DatabaseWebsitheelper(Entities db) : base(db)
        {
        }

        /// <summary>
        /// Get a list with all available website from the database 
        /// </summary>
        /// <returns>A list with all available website from the database</returns>
        public List<AppGridApplication> getWebsites()
        {
            return this.database.AppGridApplications.ToList();
        }
    }
}