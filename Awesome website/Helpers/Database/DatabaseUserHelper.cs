﻿using System.Linq;
using System.Web;
using Microsoft.AspNet.Identity;
using Awesome_website.Models;

namespace Awesome_website.Helpers.Database
{
    /// <summary>
    /// Helper class with functionallity for user related things
    /// </summary>
    public class DatabaseUserHelper : DatabaseHelper
    {
        /// <summary>
        /// The ID of the current user.
        /// </summary>
        private string userId;

        public DatabaseUserHelper(Entities db) : base(db)
        {
            this.userId = HttpContext.Current.User.Identity.GetUserId();

        }

        /// <summary>
        /// Get the data from the database relative to the current user
        /// </summary>
        /// <returns></returns>
        public AspNetUser getUserData()
        {
            return this.database.AspNetUsers.SingleOrDefault(x => x.Id == this.userId);
        }

        /// <summary>
        /// Save the new/updated user grid.
        /// </summary>
        /// <param name="grid">Grid List</param>
        public void saveUserGrid(string grid)
        {
            this.getUserData().appcenter_grid = grid;
            base.database.SaveChanges();
        }



    }
}