﻿var windowToggledProgrammatically = false;
var lampToggledProgrammatically = false;

$(function () {

    $(".lamp").change(function () {
        var clickedSwitch = $(this);        
        var id = $(this).attr('data-id');
        var state = $(this).is(":checked");

        // Do not post when we just changed the state programmatically, this will create an infinite loop that keeps toggling the switch
        if (lampToggledProgrammatically) {
            lampToggledProgrammatically = false;
            return;
        }

        $.post({
            url: LAMP_URL,
            data: {
                id,
                state
            },
            success: function(response) {

                


            },
            error: function (jqXHR, exception) {

                lampToggledProgrammatically = true;
                clickedSwitch.bootstrapToggle('toggle');
                Materialize.toast("Er is iets fout gegaan");
            }
        });

    });


    $(".window").change(function () {
        var clickedSwitch = $(this);        
        var id = $(this).attr('data-id');
        var state = $(this).is(":checked");

        // Do not post when we just changed the state programmatically, this will create an infinite loop that keeps toggling the switch
        if (windowToggledProgrammatically) {
            windowToggledProgrammatically = false;
            return;
        }

        $.post({
            url: WINDOW_URL,
            data: {
                id,
                state
            },
            success: function (response) {


            },
            error: function (jqXHR, exception) {
                
                windowToggledProgrammatically = true;
                clickedSwitch.bootstrapToggle('toggle');
                Materialize.toast("Er is iets fout gegaan");
            }
            
        });

    });



    $(".heater").change(function() {

        var value = $(this).val();
        value = value.replace('.', ',');

        $.post({
            url: HEATER_URL,
            data: {
                temperature: value
            },
            success: function (response) {


            }

        });

    });

});